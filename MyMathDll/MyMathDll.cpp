// The myPuts function writes a null-terminated string to
// the standard output device.

// The export mechanism used here is the __declspec(export)
// method supported by Microsoft Visual Studio, but any
// other export method supported by your development
// environment may be substituted.

//#include <windows.h>
#include "pch.h"
#include <iostream>

//#ifdef __cplusplus    // If used by C++ code,
//extern "C" {          // we need to export the C interface
//#endif

int Addition(int x, int y)
{
	int z;
	z = x + y;
	return z;
}
int Multiplication(int x, int y)
{
	int z;
	z = x * y;
	return z;
}
void Printstr(std::string printtest)
{
	std::cout << printtest << std::endl;
}
/*__declspec(dllexport) int __cdecl Substraction(int x, int y)
{
	int z;
	z = x - y;
	return z;
}
__declspec(dllexport) int __cdecl Division(int x, int y)
{
	int z;
	z = x / y;
	return z;
}*/

//#ifdef __cplusplus
//}
//#endif