// MyMath.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Windows.h>
#include <conio.h>
using namespace std;

typedef int(__cdecl* FunCalc)(int a, int b);
typedef void(*FunPrint)(string str);
//typedef int(__cdecl* FunSub)(int a, int b);
//typedef int(__cdecl* FunMul)(int a, int b);
//typedef int(__cdecl* FunDiv)(int a, int b);

// main class
int main()
{
	_getch();


	//Local Variable
	HMODULE hModule;
	string Printstr = "LLJASLDJASJDJQJLWEQJWE������������������������������";

	//STEP1 Load Library - Get a Handle to DLL Module
	hModule = LoadLibrary(TEXT("MyMathDll.dll"));
	if (NULL == hModule) cout << "Load Failed" << GetLastError() << endl;
	//return 0;
	cout << "Load DLL Successful" << endl;

	//STEP2 GetProcAddress - Get the Function Address
	FunCalc AdditionFun = (FunCalc)GetProcAddress(hModule, "Addition");
	FunCalc AdditionMultiply = (FunCalc)GetProcAddress(hModule, "Multiplication");
	FunPrint Funprtr = (FunPrint)GetProcAddress(hModule, "Printstr");
	//void (*returnPrint) (string);
	//returnPrint = (void(*)(string))GetProcAddress(hModule, "Printstr");
	//returnPrint(Printstr);
	Funprtr(Printstr);


	//3 Check the Function Adress is Valid
	if (NULL == AdditionFun) cout << "Function adress is not Valid: " << GetLastError() << endl;
	cout << "Function adress is Valid" << endl;

	cout << "Addition: " << AdditionFun(5, 5) << endl;
	cout << "AdditionMultiply: " << AdditionMultiply(5, 5) << endl;

	FreeLibrary(hModule);

	system("PAUSE");
	return 0;
}